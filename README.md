# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version 1.0.0
* This is <strong>UniUserProject</strong> app build for testing purposes.

### How do I get set up? ###

* The most important thing is to configure database parameters inside <strong>appsettings.json</strong> file -> <strong>UniUserProject</strong> project
* Under the <strong>Database</strong> section find <strong>ConnectionString</strong> and replace it with your connection string sql server parameters
* Please put connection string in the following format <strong>"Data Source={name of data source};Initial Catalog={db name};Integrated Security=True"</strong>
* If on your local machine you use <strong>User Id</strong> and <strong>Password</strong> please include those 2 parameters in connection string
* Under the <strong>Database</strong> section find <strong>DatabaseName</strong> and replace it with your database name -> <strong>DatabaseName</strong> must be the same as <strong>Initial Catalog</strong>
* Under the <strong>Database</strong> section find <strong>Type</strong> and replace it with <strong>SqlServer</strong> or <strong>InMemory</strong> -> <strong>SqlServer</strong> will use real Sql Server db and <strong>InMemory</strong> will use memory database
* <strong>InMemory</strong> database is for the <strong>Integration Tests</strong> instead of use real Sql Server Db

### Integration Tests ###

* The most important thing is to configure database parameters inside <strong>appsettings.json</strong> file -> <strong>UniUserProject.IntegrationTests</strong>
* Under the <strong>Database</strong> section find <strong>ConnectionString</strong> and replace it with your connection string sql server parameters
* Please put connection string in the following format <strong>"Data Source={name of data source};Initial Catalog={db name};Integrated Security=True"</strong>
* If on your local machine you use <strong>User Id</strong> and <strong>Password</strong> please include those 2 parameters in connection string
* Under the <strong>Database</strong> section find <strong>DatabaseName</strong> and replace it with your database name -> <strong>db name used in integration tests must be different from db name used for the app</strong>
* Under the <strong>Database</strong> section find <strong>Type</strong> and replace it with <strong>SqlServer</strong> or <strong>InMemory</strong> -> <strong>SqlServer</strong> will use real Sql Server db and <strong>InMemory</strong> will use memory database
* <strong>InMemory</strong> database is for the <strong>Integration Tests</strong> instead of use real Sql Server Db. <strong>But you can use Sql Server db as well since db will be removed after tests are executed</strong>

### Who do I talk to? ###

* If you have any problems running this app please contact me on my email.
* To build the app I used Visual Studio 2022, so generated .vs folder is from 2022 version. If you have any problems with that please contact me.