﻿
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UniUserProject.Controllers;
using UniUserProject.Core;
using UniUserProject.Infrastructure.Sql;
using UniUserProject.IntegrationTests;
using InMemory = UniUserProject.Infrastructure.InMemory;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddMediatR(typeof(HomeController).Assembly);
builder.Services.AddAutoMapper(typeof(HomeController).Assembly);

builder.Services.AddAntiforgery(t =>
{
    t.Cookie.Name = AntiForgeryTokenExtractor.AntiForgeryCookieName;
    t.FormFieldName = AntiForgeryTokenExtractor.AntiForgeryFieldName;
});

builder.Services.AddMvc().AddApplicationPart(typeof(HomeController).Assembly).AddDataAnnotationsLocalization();

var databaseType = builder.Configuration.GetValue<string>("Database:Type");
if (databaseType == "SqlServer")
{
    var connectionString = builder.Configuration.GetValue<string>("Database:ConnectionString");
    var databaseName = builder.Configuration.GetValue<string>("Database:DatabaseName");
    builder.Services.AddSingleton(new UserSqlDbContext(connectionString, databaseName));
    builder.Services.AddScoped<IUserRepository, UserRepository>();
}
else if (databaseType == "InMemory")
{
    builder.Services.AddSingleton(new InMemory.UserInMemoryDbContext());
    builder.Services.AddScoped<IUserRepository, InMemory.UserRepository>();
}

var app = builder.Build();

app.UseRouting();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();

public partial class Program { }
