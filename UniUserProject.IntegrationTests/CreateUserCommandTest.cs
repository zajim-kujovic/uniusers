using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Net.Http.Headers;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using UniUserProject.Core;
using UniUserProject.Infrastructure.Sql;
using Xunit;

namespace UniUserProject.IntegrationTests
{
    public class CreateUserCommandTest : IClassFixture<WebApplicationFactory<Program>>
    {
        private readonly WebApplicationFactory<Program> _factory;

        public CreateUserCommandTest(WebApplicationFactory<Program> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task CreateUser_Command_Executed_WithSuccess_ByAddingData_IntoDb()
        {
            // Arrange
            string username = "zajim";
            var client = _factory.CreateClient();
            
            // Act
            var initResponse = await client.GetAsync("/Home/Register");
            var antiForgeryValues = await AntiForgeryTokenExtractor.ExtractAntiForgeryValues(initResponse);

            var command = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("Username", username),
                new KeyValuePair<string, string>("Password", "password1234"),
                new KeyValuePair<string, string>("ConfirmPassword", "password1234"),
                new KeyValuePair<string, string>(AntiForgeryTokenExtractor.AntiForgeryFieldName, antiForgeryValues.fieldValue)
            };

            var content = new FormUrlEncodedContent(command);
            content.Headers.Add("Cookie", new CookieHeaderValue(AntiForgeryTokenExtractor.AntiForgeryCookieName, antiForgeryValues.cookieValue).ToString());

            var result = await client.PostAsync("Home/Register", content);
            
            // Assert
            result.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);

            var response = await result.Content.ReadAsStringAsync();
            Assert.NotNull(response);
            var serviceProvider = _factory.Services.CreateScope().ServiceProvider;
            var userRepository = serviceProvider.GetRequiredService<IUserRepository>();
            var userExists = await userRepository.DoesExistsByUsername(username).ConfigureAwait(false);
            Assert.True(userExists);

            var dbContext = serviceProvider.GetService<UserSqlDbContext>();
            if (dbContext != null)
            {
                dbContext.DropDatabase();
            }
        }
    }
}