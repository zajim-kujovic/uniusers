﻿using System.Net;

namespace UniUserProject.Models
{
    public class Response<T>
    {
        public Response(HttpStatusCode httpStatusCode)
        {
            this.StatusCode = httpStatusCode;
        }

        public Response(HttpStatusCode httpStatusCode, T? result)
        {
            this.StatusCode = httpStatusCode;
            this.Result = result;
        }

        public Response(HttpStatusCode httpStatusCode, LogLevel logLevel, string? logMessage, string? userFriendlyMessage)
        {
            this.StatusCode = httpStatusCode;
            this.LogMessage = logMessage;
            this.UserFriendlyMessage = userFriendlyMessage;
            this.LogLevel = logLevel;
        }

        public HttpStatusCode StatusCode { get; }

        public T? Result { get; }

        public string? LogMessage { get; }

        public string? UserFriendlyMessage { get; set; }

        public LogLevel LogLevel { get; set; }
    }
}
