﻿using System.ComponentModel.DataAnnotations;

namespace UniUserProject.Models
{
    public class RegisterUserViewModel
    {
        [Display(Name = "Username")]
        [Required]
        [StringLength(30)]
        [MinLength(5)]
        [MaxLength(100)]
        public string? Username { get; set; }

        [Display(Name = "Password")]
        [Required]
        [StringLength(30)]
        [DataType(DataType.Password)]
        [MinLength(8)]
        [MaxLength(100)]
        public string? Password { get; set; }

        [Display(Name = "Confirm Password")]
        [Required]
        [StringLength(30)]
        [DataType(DataType.Password)]
        [MinLength(8)]
        [MaxLength(100)]
        [Compare(nameof(Password), ErrorMessage = "Confirm password doesn't match, Type again!")]
        public string? ConfirmPassword { get; set; }
    }
}
