using MediatR;
using System.Reflection;
using UniUserProject.Core;
using UniUserProject.Infrastructure.Sql;
using InMemory = UniUserProject.Infrastructure.InMemory;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddMediatR(Assembly.GetExecutingAssembly());
builder.Services.AddAutoMapper(Assembly.GetExecutingAssembly());

var databaseType = builder.Configuration.GetValue<string>("Database:Type");
if (databaseType == "SqlServer")
{
    var connectionString = builder.Configuration.GetValue<string>("Database:ConnectionString");
    var databaseName = builder.Configuration.GetValue<string>("Database:DatabaseName");
    builder.Services.AddSingleton(new UserSqlDbContext(connectionString, databaseName));
    builder.Services.AddScoped<IUserRepository, UserRepository>();
}
else if (databaseType == "InMemory")
{
    builder.Services.AddSingleton(new InMemory.UserInMemoryDbContext());
    builder.Services.AddScoped<IUserRepository, InMemory.UserRepository>();
}


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
