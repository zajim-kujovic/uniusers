﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using UniUserProject.Commands.User;
using UniUserProject.Models;

namespace UniUserProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMediator _mediator;

        public HomeController(ILogger<HomeController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View(nameof(Index));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register([Bind("Username,Password,ConfirmPassword")] RegisterUserViewModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return View(nameof(Index), userModel);
            }
            
            var result = await _mediator.Send(new CreateUserCommand(userModel.Username, userModel.Password));

            if(result.StatusCode == HttpStatusCode.OK)
            {
                return RedirectToAction(nameof(Index));
            }

            return Error(result);
        }

        private IActionResult Error(Response<string> response)
        {
            switch (response.LogLevel)
            {
                case LogLevel.Trace:
                    _logger.LogTrace(response.LogMessage);
                    break;
                case LogLevel.Debug:
                    _logger.LogDebug(response.LogMessage);
                    break;
                case LogLevel.Information:
                    _logger.LogInformation(response.LogMessage);
                    break;
                case LogLevel.Warning:
                    _logger.LogWarning(response.LogMessage);
                    break;
                case LogLevel.Error:
                    _logger.LogError(response.LogMessage);
                    break;
                case LogLevel.Critical:
                    _logger.LogCritical(response.LogMessage);
                    break;
                case LogLevel.None:
                default:
                    _logger.LogInformation(response.LogMessage);
                    break;
            }
            return View(nameof(Error), new ErrorViewModel { Message = response.UserFriendlyMessage });
        }
    }
}