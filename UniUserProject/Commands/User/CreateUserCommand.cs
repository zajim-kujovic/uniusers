﻿using MediatR;
using UniUserProject.Models;

namespace UniUserProject.Commands.User
{
    public class CreateUserCommand : IRequest<Response<string>>
    {
        public CreateUserCommand(string? username, string? password)
        {
            Username = username;
            Password = password;
        }

        public string? Username { get; }
        public string? Password { get; }
    }
}
