﻿using MediatR;
using System.Net;
using UniUserProject.Core;
using UniUserProject.Models;

namespace UniUserProject.Commands.User
{
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, Response<string>>
    {
        private readonly IUserRepository _userRepository;

        public CreateUserCommandHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<Response<string>> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var doesUserExists = await _userRepository.DoesExistsByUsername(request.Username).ConfigureAwait(false);
            if (doesUserExists)
            {
                return new Response<string>(
                    HttpStatusCode.BadRequest,
                    LogLevel.Warning,
                    $"User with the this username {request.Username} already exists",
                    "User with the same username already exists");
            }

            var user = Core.User.Create(request.Username, request.Password);
            await _userRepository.Insert(user).ConfigureAwait(false);

            return new Response<string>(HttpStatusCode.OK, user.Id);
        }
    }
}
