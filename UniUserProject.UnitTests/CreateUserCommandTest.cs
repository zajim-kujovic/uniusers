using Microsoft.Extensions.Logging;
using Moq;
using System.Net;
using System.Threading.Tasks;
using UniUserProject.Commands.User;
using UniUserProject.Core;
using Xunit;

namespace UniUserProject.UnitTests
{
    public class CreateUserCommandTest
    {
        [Fact]
        public async Task CreateUser_Command_Failed_Because_TheSameUsername_AlreadyExists()
        {
            // Arrange
            string username = "zajim";
            string password = "password";
            var command = new CreateUserCommand(username, password);

            var userRepositoryMock = new Mock<IUserRepository>();
            userRepositoryMock.Setup(u => u.DoesExistsByUsername(username)).ReturnsAsync(true);
            var handler = new CreateUserCommandHandler(userRepositoryMock.Object);

            // Act
            var result = await handler.Handle(command, default);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
            Assert.Equal(LogLevel.Warning, result.LogLevel);
            Assert.Equal($"User with the this username {command.Username} already exists", result.LogMessage);
            Assert.Equal("User with the same username already exists", result.UserFriendlyMessage);
            userRepositoryMock.Verify(u => u.DoesExistsByUsername(username), Times.Once);
            userRepositoryMock.Verify(u => u.Insert(It.IsAny<User>()), Times.Never);
        }

        [Fact]
        public async Task CreateUser_Command_Executed_WithSuccess_ByAddingData_IntoDb()
        {
            // Arrange
            string username = "zajim";
            string password = "password";
            var command = new CreateUserCommand(username, password);

            var userRepositoryMock = new Mock<IUserRepository>();
            userRepositoryMock.Setup(u => u.DoesExistsByUsername(username)).ReturnsAsync(false);
            var handler = new CreateUserCommandHandler(userRepositoryMock.Object);

            // Act
            var result = await handler.Handle(command, default);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.Null(result.LogMessage);
            Assert.Null(result.UserFriendlyMessage);
            userRepositoryMock.Verify(u => u.DoesExistsByUsername(username), Times.Once);
            userRepositoryMock.Verify(u => u.Insert(It.IsAny<User>()), Times.Once);
        }
    }
}