﻿using UniUserProject.Core;
using Xunit;

namespace UniUserProject.UnitTests
{
    public class UserModelTest
    {
        [Fact]
        public void CreateUser_WithSuccess()
        {
            // Arrange
            string? username = "zajim";
            string? password = "password";

            // Act
            var user = User.Create(username, password);

            // Assert
            Assert.NotNull(user.Id);
            Assert.NotEmpty(user.Id);
            Assert.Equal(username, user.Username);
            Assert.NotEqual(password, user.Password);
            Assert.True(user.VerifyPassword(password));
            Assert.False(user.VerifyPassword("false password"));
        }
    }
}
