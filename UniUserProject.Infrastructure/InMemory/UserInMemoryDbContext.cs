﻿using UniUserProject.Core;

namespace UniUserProject.Infrastructure.InMemory
{
    public class UserInMemoryDbContext
    {
        public UserInMemoryDbContext()
        {
            Users = new List<User>();
        }

        public IList<User> Users { get; }
    }
}
