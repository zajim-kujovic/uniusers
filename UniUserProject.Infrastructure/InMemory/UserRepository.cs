﻿using UniUserProject.Core;

namespace UniUserProject.Infrastructure.InMemory
{
    public class UserRepository : IUserRepository
    {
        private readonly UserInMemoryDbContext _dbContext;

        public UserRepository(UserInMemoryDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<bool> DoesExistsByUsername(string? username)
        {
            return Task.Run(() => _dbContext.Users.Any(u => u.Username == username));
        }

        public Task Insert(User user)
        {
            return Task.Run(() => _dbContext.Users.Add(user));
        }
    }
}