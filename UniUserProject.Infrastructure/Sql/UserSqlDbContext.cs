﻿using System.Data;
using System.Data.SqlClient;
using UniUserProject.Core;

namespace UniUserProject.Infrastructure.Sql
{
    public class UserSqlDbContext
    {
        private const string MasterDbName = "master";
        private readonly string _databaseName;
        private readonly string _connectionString;

        public UserSqlDbContext(string connectionString, string databaseName)
        {
            _connectionString = connectionString;
            _databaseName = databaseName;
            string masterConnectionString = connectionString.Replace(databaseName, MasterDbName);
            CreateDatabase(masterConnectionString);
            CreateTables();
        }

        public string ConnectionString => _connectionString;

        public string UserTableName => "[dbo].[users]";

        public bool DropDatabase()
        {
            using SqlConnection connection = new SqlConnection(_connectionString);
            bool isExits = CheckDatabaseExists(connection, _databaseName);
            if (isExits)
            {
                string dropDatabase = $"USE {MasterDbName}; DROP DATABASE {_databaseName};";
                using SqlCommand command = new SqlCommand(dropDatabase, connection);
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
            }
            return true;
        }

        private bool CreateDatabase(string connectionString)
        {
            using SqlConnection connection = new SqlConnection(connectionString);
            bool isExits = CheckDatabaseExists(connection, _databaseName);
            if (!isExits)
            {
                string createDatabase = $"CREATE DATABASE {_databaseName};";
                using SqlCommand command = new SqlCommand(createDatabase, connection);
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    return false;
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
            }
            return true;
        }

        private bool CheckDatabaseExists(SqlConnection tmpConn, string databaseName)
        {
            string sqlCreateDBQuery;
            bool result = false;

            try
            {
                sqlCreateDBQuery = string.Format("SELECT database_id FROM sys.databases WHERE Name = @databaseName");
                using SqlCommand sqlCmd = new SqlCommand(sqlCreateDBQuery, tmpConn);
                sqlCmd.Parameters.AddWithValue("@databaseName", databaseName);
                tmpConn.Open();
                object resultObj = sqlCmd.ExecuteScalar();
                int databaseID = 0;
                if (resultObj != null)
                {
                    int.TryParse(resultObj.ToString(), out databaseID);
                }
                tmpConn.Close();
                result = (databaseID > 0);
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }

        private bool CreateTables()
        {
            string createTable = $"IF OBJECT_ID(N'[dbo].[{nameof(User)}]', N'U') IS NULL " +
                "BEGIN " +
                $"CREATE TABLE {UserTableName} (" +
                $"[{nameof(User.Id)}] NVARCHAR(36)," +
                $"[{nameof(User.Username)}] NVARCHAR(100)," +
                $"[{nameof(User.Password)}] NVARCHAR(100)" +
                $") " +
                "END;";
            using SqlConnection connection = new SqlConnection(_connectionString);
            using SqlCommand command = new SqlCommand(createTable, connection);
            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return true;
        }
    }
}
