﻿using System.Data;
using System.Data.SqlClient;
using UniUserProject.Core;

namespace UniUserProject.Infrastructure.Sql
{
    public class UserRepository : IUserRepository
    {
        private readonly UserSqlDbContext _dbContext;

        public UserRepository(UserSqlDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> DoesExistsByUsername(string? username)
        {
            using SqlConnection connection = new SqlConnection(_dbContext.ConnectionString);
            string command = $"SELECT Id FROM {_dbContext.UserTableName} WHERE Username = @username";
            using SqlCommand sqlCommand = new SqlCommand(command, connection);
            sqlCommand.Parameters.AddWithValue("@username", username);
            bool result = false;
            try
            {
                _ = connection.OpenAsync().ConfigureAwait(false);
                var resultObj = await sqlCommand.ExecuteScalarAsync();
                result = resultObj != null && !string.IsNullOrWhiteSpace(resultObj.ToString());
            }
            catch (Exception)
            {
                result = false;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    await connection.CloseAsync();
                }
            }
            return result;
        }

        public async Task Insert(User user)
        {
            using SqlConnection connection = new SqlConnection(_dbContext.ConnectionString);
            string command = $"INSERT INTO {_dbContext.UserTableName} ({nameof(User.Id)}, {nameof(User.Username)}, {nameof(User.Password)}) VALUES " +
                $"(@id, @username, @password)";
            using SqlCommand sqlCommand = new SqlCommand(command, connection);
            sqlCommand.Parameters.AddWithValue("@id", user.Id);
            sqlCommand.Parameters.AddWithValue("@username", user.Username);
            sqlCommand.Parameters.AddWithValue("@password", user.Password);
            try
            {
                await connection.OpenAsync().ConfigureAwait(false);
                await sqlCommand.ExecuteNonQueryAsync();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    await connection.CloseAsync();
                }
            }
        }
    }
}