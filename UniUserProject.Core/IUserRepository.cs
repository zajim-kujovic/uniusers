﻿using System.Threading.Tasks;

namespace UniUserProject.Core
{
    public interface IUserRepository
    {
        Task Insert(User user);
        Task<bool> DoesExistsByUsername(string? username);
    }
}
