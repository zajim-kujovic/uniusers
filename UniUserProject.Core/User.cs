﻿using System;

namespace UniUserProject.Core
{
    public class User
    {
        private User()
        {
        }

        public string Id { get; private set; } = Guid.NewGuid().ToString();

        public string? Username { get; private set; }

        public string? Password { get; private set; }

        public static User Create(string? username, string? password)
        {
            var hashedPassword = Convert.ToBase64String(new PasswordHash(password).ToArray());
            return new User
            {
                Username = username,
                Password = hashedPassword
            };
        }

        public bool VerifyPassword(string password)
        {
            return new PasswordHash(Convert.FromBase64String(Password)).Verify(password);
        }
    }
}
